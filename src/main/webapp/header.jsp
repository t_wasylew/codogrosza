<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Industrious by TEMPLATED</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">

<!-- Header -->
<header id="header">
    <a class="logo" href="asdsa.html">E-pracownik</a>
    <nav>
        <a href="#menu">Menu</a>
    </nav>
</header>

<!-- Nav -->
<nav id="menu">
    <ul class="links">
        <li><a href="index.jsp">Home</a></li>
        <li><a href="<c:url value="/userLogin"/>">Sign in</a></li>
        <li><a href="register.jsp">Register</a></li>
        <li><a href="<c:url value="/userLogin"/>">My account</a></li>
    </ul>
</nav>

<!-- Banner -->
<section id="banner">
    <div class="inner">
        <h1>E-pracownik</h1>
        <p>Simple webApplication to control your work time and excpected salary<br />
            made by Tomasz Wasylew</p>
    </div>
    <video autoplay loop muted playsinline src="images/banner.mp4"></video>
</section>
