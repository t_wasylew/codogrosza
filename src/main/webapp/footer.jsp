<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: wasyl
  Date: 24.03.2018
  Time: 16:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Footer -->
<footer id="footer">
    <section id="cta" class="wrapper">
        <div class="inner">
            <div class="content">
                <section>
                    <h3>Tomasz Wasylew</h3>
                    <h2>Junior Java Developer candidate</h2>
                    <p>Hi! Here is your candidate for junior java developer. I am an incurable optimist whose life has changed the course when he discovered his greatest passion - programming. I am an ambitious person who learn quickly and accurately doing the tasks entrusted to me.</p>
                </section>
                <section>
                    <h4>Useful links</h4>
                    <section class="plain">
                        <a href="https://www.linkedin.com/in/tomasz-wasylew-aa8b69153/"><i class="icon fa-linkedin">&nbsp;</i>Linkedin</a>
                        <a href="https://bitbucket.org/t_wasylew/"><i class="icon fa-github">&nbsp;</i>Github</a>
                        <a href="https://www.facebook.com/tomek.wasylew"><i class="icon fa-facebook">&nbsp;</i>Facebook</a>
                    </section>
                </section>
            </div>
            <div class="copyright">
                <small class="d-block mb-3 text-muted">Copyright © <%= LocalDate.now().getYear()%> Tomasz Wasylew. All Rights Reserved</small>
            </div>
        </div>
    </section>
</footer>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>

