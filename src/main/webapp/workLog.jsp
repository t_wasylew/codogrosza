<%@ page import="main.java.model.Rate" %>
<%@ page import="java.util.List" %>
<%@ page import="main.java.model.WorkLog" %>
<%@ page import="java.time.temporal.ChronoUnit" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="headerForWorkLog.jsp"/>
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light">
    <div class="col-md-10 p-lg-10 mx-auto my-10">
        <h1 class="font-weight-normal">Work Log List</h1>

        <div class="row col-lg-12">
            <div class="col-md-3"><h4>Start date</h4></div>
            <div class="col-md-3"><h4>End date </h4></div>
            <div class="col-md-3"><h4>Rate</h4></div>
            <div class="col-md-3"><h4>Hours</h4></div>
        </div>
            <%
            List<WorkLog> workLogList = (List<WorkLog>) request.getAttribute("workLogList");
            StringBuilder builder = new StringBuilder();

            int i = 0;
            if(workLogList != null){
            for (WorkLog workLog : workLogList) {
                if (i % 2 == 0) {
                    builder.append("<div class=\"row white\">");
                } else {
                    builder.append("<div class=\"row black\">");
                }
                builder.append("<div class=\"col-md-3\">").append(workLog.getWorkStartsAt()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(workLog.getWorkEndsAt()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(workLog.getRate().getWorkRate()).append(": ").append(workLog.getRate().getRateValue()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(ChronoUnit.HOURS.between(workLog.getWorkStartsAt(),workLog.getWorkEndsAt())).append("</div>");
                builder.append("</div>");
                i++;
            }
            }else {
                builder.append("<div class=\"col-mad-12\">").append("You don't have any workLogs yet.").append("</div>");
            }
            out.print(builder.toString());
        %>
        <h1>Add New Work Log</h1>

        <form class="form-horizontal" method="post" action="<c:url value="/workLog"/>">
            <div class="form-group">
                <label class="control-label col-sm-2">Rate</label>
                <div class="col-sm-5">
                    <select class="form-control" name="rate" required>
                        <%
                            List<Rate> rateList = (List<Rate>) request.getAttribute("rateList");
                            for (Rate rate : rateList) {
                                out.println("<option value=\"" + rate.getWorkRate() + "\" name=\"" + rate.getWorkRate() + "\">" + rate.getWorkRate() + ": " + rate.getRateValue() + "</option>");
                            }
                        %>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Start date</label>
                <div class="col-sm-5">
                    <input class="form-control" type="datetime-local" name="workStartsAt" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">End date</label>
                <div class="col-sm-5">
                    <input class="form-control" type="datetime-local" name="workEndsAt" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Add</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </form>
<jsp:include page="footer.jsp"/>