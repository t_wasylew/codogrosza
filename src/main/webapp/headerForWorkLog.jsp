<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Industrious by TEMPLATED</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">

<!-- Header -->
<header id="header">
    <a class="logo" href="asdsa.html">E-pracownik</a>
    <nav>
        <a href="#menu">Menu</a>
    </nav>
</header>

<!-- Nav -->
<nav id="menu">
    <ul class="links">
        <li><a href="index.jsp">Home</a></li>
        <li><a href="<c:url value="/workLog"/>">Add new WorkLog</a></li>
        <li><a href="<c:url value="/rateServlet"/>">Add new rate</a></li>
        <li><a href="<c:url value="/userLogin"/>">My Account</a></li>
        <li><a href="<c:url value="/userLogout"/>">Logout</a></li>
    </ul>
</nav>
<section class="wrapper">
    <div class="inner">
        <header class="special">
            <h2>Welcome!</h2>
            <p>Menu</p>
        </header>
        <div class="highlights">
            <section>
                <div class="content">
                    <header>
                        <a href="<c:url value="/workLog"/>" class="icon fa-address-card"><span class="label">Add new WorkLog</span></a>
                        <h3>Add new WorkLog</h3>
                    </header>
                </div>
            </section>
            <section>
                <div class="content">
                    <header>
                        <a href="<c:url value="/rateServlet"/>" class="icon fa-address-book"><span class="label">Add new rate</span></a>
                        <h3>Add new rate</h3>
                    </header>
                </div>
            </section>
            <section>
                <div class="content">
                    <header>
                        <a href="<c:url value="/userLogin"/>" class="icon fa-vcard"><span class="label">My Account</span></a>
                        <h3>My Account</h3>
                    </header>
                </div>
            </section>
            <section>
                <div class="content">
                    <header>
                        <a href="<c:url value="/userLogout"/>" class="icon fa-sign-out"><span class="label">Logout</span></a>
                        <h3>Logout</h3>
                    </header>
                </div>
            </section>
        </div>
    </div>
</section>


