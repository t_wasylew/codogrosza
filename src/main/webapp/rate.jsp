<%@ page import="main.java.model.Rate" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="headerForWorkLog.jsp"/>
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light">
    <div class="col-md-10 p-lg-10 mx-auto my-10">
        <h1 class="font-weight-normal">Rate List</h1>

        <div class="row col-lg-12">
            <div class="col-md-3"><h4>Income Type</h4></div>
            <div class="col-md-3"><h4>Hour Rate/Salary</h4></div>
        </div>
            <%
            List<Rate> rateList = (List<Rate>) request.getAttribute("rateList");
            StringBuilder builder = new StringBuilder();

            int i = 0;
            if(rateList != null){
            for (Rate rate : rateList) {
                if (i % 2 == 0) {
                    builder.append("<div class=\"row white\">");
                } else {
                    builder.append("<div class=\"row black\">");
                }
                builder.append("<div class=\"col-md-3\">").append(rate.getWorkRate()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(rate.getRateValue()).append("</div>");
                builder.append("</div>");
                i++;
            }
            }else {
                builder.append("<div class=\"col-mad-12\">").append("You don't have any rates yet.").append("</div>");
            }
            out.print(builder.toString());
        %>
<h1> Add new rate !!!</h1>
<form class="form-horizontal" method="post" action="<c:url value="/rateServlet"/>">
    <div class="form-group">
        <label class="control-label col-sm-2">Income Type</label>
        <div class="col-sm-5">
            <select class="form-control" name="incomeType" required>
                <option value="Hour rate">Hour rate</option>
                <option value="Monthly">Monthly</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Salary/Hour rate</label>
        <div class="col-sm-5">
            <input class="form-control" type="number" name="incomeValue" placeholder="Enter your salary or hour rate"
                   required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="primary">Submit</button>
            <button type="reset" class="primary">Reset</button>

        </div>
    </div>
</form>
<jsp:include page="footer.jsp"/>