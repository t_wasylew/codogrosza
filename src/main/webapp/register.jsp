<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>
<h1> Register</h1>
    <form class="form-horizontal" method="post" action="<c:url value="/userRegister"/>">
        <div class="form-group">
            <label class="control-label col-sm-2">Name</label>
            <div class="col-sm-5">
                <input class="form-control" type="text" name="name" placeholder="Enter name" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Login</label>
            <div class="col-sm-5">
                <input class="form-control" type="text" name="login"
                       <%
                       String error = (String) request.getAttribute("error");
                       if ("loginTaken".equals(error)){
                           %> placeholder="Login already taken" style="border-color: red"<%
                       }else {%>
                       placeholder="Enter login"
                       <%}%>
                       required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Password</label>
            <div class="col-sm-5">
                <input class="form-control" type="password" name="password" placeholder="Enter password" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Income Type</label>
            <div class="col-sm-5">
                <select class="form-control" name="incomeType" required>
                    <option value="Hour rate">Hour rate</option>
                    <option value="Monthly">Monthly</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Salary/Hour rate</label>
            <div class="col-sm-5">
                <input class="col-12-medium" type="number" name="incomeValue" placeholder="Enter your salary or hour rate" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="primary">Submit</button>
                <button type="reset" class="primary">Reset</button>
            </div>
        </div>
    </form>

<jsp:include page="footer.jsp"/>
