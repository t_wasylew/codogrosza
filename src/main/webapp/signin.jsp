<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>
<h1> Sign IN!</h1>

<form class="form-horizontal" method="post" action="<c:url value="/userLogin"/>">
    <div class="form-group">
        <label class="control-label col-sm-2">Login</label>
        <div class="col-sm-5">
            <input class="form-control" type="text" name="login"
                <% String error = (String) request.getAttribute("error");
                       if ("errorLogin".equals(error)){%>
                   placeholder="Wrong login or password" style="border-color: red"<%
                       }else {%>
                   placeholder="Enter login"
                <%}%>
                   required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Password</label>
        <div class="col-sm-5">
            <input class="form-control" type="password" name="password" placeholder="Enter your password" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Login</button>
        </div>
    </div>
</form>
<jsp:include page="footer.jsp"/>
