<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>
<section class="wrapper">
    <div class="inner">
        <header class="special">
            <h2>Welcome!</h2>
            <p>Menu</p>
        </header>
        <div class="highlights">
            <section>
                <div class="content">
                    <header>
                        <a href="<c:url value="/userLogin"/>" class="icon fa-sign-in"><span class="label">Sign in</span></a>
                        <h3>Sign in</h3>
                    </header>
                </div>
            </section>
            <section>
                <div class="content">
                    <header>
                        <a href="register.jsp" class="icon fa-registered"><span class="label">Register</span></a>
                        <h3>Register</h3>
                    </header>
                </div>
            </section>
            <section>
                <div class="content">
                    <header>
                        <a href="<c:url value="/userLogin"/>" class="icon fa-vcard"><span class="label">My account</span></a>
                        <h3>My Account</h3>
                    </header>
                </div>
            </section>
        </div>
    </div>
</section>
<jsp:include page="footer.jsp"/>