package main.java.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class WorkLog {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long workLogID;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    @OneToOne(fetch = FetchType.EAGER)
    private Rate rate;

    @Column
    private LocalDateTime workStartsAt;

    @Column
    private LocalDateTime workEndsAt;

    public WorkLog() {
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public Long getWorkLogID() {
        return workLogID;
    }

    public void setWorkLogID(Long workLogID) {
        this.workLogID = workLogID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getWorkStartsAt() {
        return workStartsAt;
    }

    public void setWorkStartsAt(LocalDateTime workStartsAt) {
        this.workStartsAt = workStartsAt;
    }

    public LocalDateTime getWorkEndsAt() {
        return workEndsAt;
    }

    public void setWorkEndsAt(LocalDateTime workEndsAt) {
        this.workEndsAt = workEndsAt;
    }
}
