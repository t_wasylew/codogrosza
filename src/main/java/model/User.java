package main.java.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {

    @Id
    @Column
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long userID;

    @Column
    private String name;

    @Column(unique = true)
    private String login;

    @Column
    private String passwordHash;

    @OneToMany
    private List<Rate> hourRate;
    
    public User() {
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public List<Rate> getHourRate() {
        return hourRate;
    }

    public void setHourRate(List<Rate> hourRate) {
        this.hourRate = hourRate;
    }

}
