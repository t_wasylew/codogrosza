package main.java.dao;

import main.java.model.WorkLog;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;


import java.util.List;

public class WorkLogDAO {
    public void addWorkLog(WorkLog workLog) {
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try{
        session.saveOrUpdate(workLog);
        }finally {
            transaction.commit();
            session.flush();
            session.close();
        }
    }

    public List<WorkLog> getAllWorkLogs(Long userID) {
        List<WorkLog> workLogList;
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            System.out.println("Szukam loga z: " + userID);
            workLogList = session.createCriteria(WorkLog.class).add(Restrictions.eq("user.userID", userID)).list();
            if (workLogList == null || workLogList.isEmpty()) {
                return null;
            } else {
                return workLogList;
            }
        } finally {
            transaction.commit();
            session.flush();
            session.close();
        }
    }
}
