package main.java.dao;

import main.java.model.Rate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class RateDAO {

    public List<Rate> getRateListByUserID(Long userID) {
        List<Rate> rateList;
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            System.out.println("Szukam loga z: " + userID);
            Query query = session.createQuery("FROM Rate WHERE userID='" + userID + "'");
            rateList = query.list();
            if (rateList == null || rateList.isEmpty()) {
                return null;
            } else {
                return rateList;
            }
        } finally {
            transaction.commit();
            session.flush();
            session.close();
        }
    }
}
