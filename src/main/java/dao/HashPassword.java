package main.java.dao;

import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashPassword {

    public String passwordHash(String password) {
        String hashed = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] encrypted = digest.digest(password.getBytes());
            hashed = DigestUtils.md5Hex(encrypted).toLowerCase();
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
        }
        return hashed;
    }
}
