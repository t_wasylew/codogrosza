package main.java.dao;

import main.java.model.User;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class UserFromCookie {

    private UserDAO userDAO = new UserDAO();

    public User getUserFromCookie(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (userDAO.getUserByLogin(cookie.getName()) != null) {
                    return userDAO.getUserByLogin(cookie.getName());
                }
            }
        }
        return null;
    }
}
