package main.java.dao;


import main.java.model.Rate;
import main.java.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserDAO {

    public void addOrUpdateUser(User user, Rate rate) {
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try{
        session.saveOrUpdate(user);
        session.saveOrUpdate(rate);
        }finally {
            transaction.commit();
            session.flush();
            session.close();
        }
    }

    public User getUserByLogin(String login) {
        User user;
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Query query = session.createQuery("FROM User WHERE login='" + login + "'");
            if (query.list().isEmpty()) {
                return null;
            } else {
                user = (User) query.uniqueResult();
                return user;
            }
        }finally {
            transaction.commit();
            session.flush();
            session.close();
        }
    }
}
