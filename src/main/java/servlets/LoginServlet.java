package main.java.servlets;

import main.java.dao.HashPassword;
import main.java.dao.UserDAO;
import main.java.dao.UserFromCookie;
import main.java.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/userLogin")
public class LoginServlet extends HttpServlet {
    private UserDAO userDAO = new UserDAO();
    private HashPassword hashPassword = new HashPassword();
    private UserFromCookie userFromCookie = new UserFromCookie();
    private User userFromDB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String page = "signin.jsp";
        try {
            if (userFromCookie.getUserFromCookie(req) != null) {
                page = "userAccount.jsp";
            }
            resp.sendRedirect(page);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String page;
        try {
            String login = req.getParameter("login");
            String password = req.getParameter("password");

            userFromDB = userDAO.getUserByLogin(login);
            if (userFromDB != null && userFromDB.getPasswordHash().equals(hashPassword.passwordHash(password))) {
                page = "/userAccount.jsp";
                Cookie cookie = new Cookie(login, password);
                cookie.setMaxAge(5 * 60);
                resp.addCookie(cookie);
            } else {
                page = "/signin.jsp";
                req.setAttribute("error", "errorLogin");
            }
            req.getRequestDispatcher(page).forward(req, resp);
        } catch (IOException | ServletException exception) {
            exception.printStackTrace();
        }
    }
}
