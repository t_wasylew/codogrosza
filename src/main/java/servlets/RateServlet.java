package main.java.servlets;



import main.java.dao.RateDAO;
import main.java.dao.UserDAO;
import main.java.dao.UserFromCookie;
import main.java.model.Rate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/rateServlet")
public class RateServlet extends HttpServlet {
    private UserDAO userDAO = new UserDAO();
    private RateDAO rateDAO = new RateDAO();
    private UserFromCookie userFromCookie = new UserFromCookie();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            req.setAttribute("rateList", getRates(req));
            req.getRequestDispatcher("/rate.jsp").forward(req, resp);
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }

    private List<Rate> getRates(HttpServletRequest req) {
        return rateDAO.getRateListByUserID(userFromCookie.getUserFromCookie(req).getUserID());
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String page;
        try {

            String incomeType = req.getParameter("incomeType");
            Double salary = Double.parseDouble(req.getParameter("incomeValue"));
            if (userFromCookie.getUserFromCookie(req) != null) {
                Rate rate = new Rate();
                rate.setWorkRate(incomeType);
                rate.setRateValue(salary);
                rate.setUser(userFromCookie.getUserFromCookie(req));
                userDAO.addOrUpdateUser(userFromCookie.getUserFromCookie(req),rate);
                page = "/rate.jsp";
            } else {
                page = "/index.jsp";
            }

            req.setAttribute("rateList", getRates(req));
            req.getRequestDispatcher(page).forward(req, resp);

        } catch (ServletException | IOException exception) {
            exception.printStackTrace();
        }
    }
}
