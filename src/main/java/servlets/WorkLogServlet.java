package main.java.servlets;

import main.java.dao.RateDAO;
import main.java.dao.WorkLogDAO;
import main.java.dao.UserFromCookie;
import main.java.model.Rate;
import main.java.model.WorkLog;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(urlPatterns = "/workLog")
public class WorkLogServlet extends HttpServlet {
    private WorkLogDAO workLogDAO = new WorkLogDAO();
    private UserFromCookie userFromCookie = new UserFromCookie();
    private RateDAO rateDAO = new RateDAO();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            System.out.println("Przekazuje zapytanie do workLogDAO");
            req.setAttribute("workLogList", getWorkLogs(req));
            req.setAttribute("rateList", getRates(req));
            req.getRequestDispatcher("/workLog.jsp").forward(req, resp);
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }

    private List<Rate> getRates(HttpServletRequest req) {
        return rateDAO.getRateListByUserID(userFromCookie.getUserFromCookie(req).getUserID());
    }

    private List<WorkLog> getWorkLogs(HttpServletRequest req) {
        return workLogDAO.getAllWorkLogs(userFromCookie.getUserFromCookie(req).getUserID());
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {

        try {
            String rate = req.getParameter("rate");
            String workStarts = req.getParameter("workStartsAt");
            String workEnds = req.getParameter("workEndsAt");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            LocalDateTime workStartAt = LocalDateTime.parse(workStarts, formatter);
            LocalDateTime workEndsAt = LocalDateTime.parse(workEnds, formatter);

            WorkLog workLog = new WorkLog();
            workLog.setRate(getRate(req, rate));
            workLog.setWorkStartsAt(workStartAt);
            workLog.setWorkEndsAt(workEndsAt);
            workLog.setUser(userFromCookie.getUserFromCookie(req));
            workLogDAO.addWorkLog(workLog);

            req.setAttribute("workLogList", getWorkLogs(req));
            req.setAttribute("rateList", getRates(req));
            req.getRequestDispatcher("/workLog.jsp").forward(req, resp);
        } catch (ServletException | IOException exception) {
            exception.printStackTrace();
        }
    }

    private Rate getRate(HttpServletRequest req, String rate) {
        for (Rate r : getRates(req)) {
            if (r.getWorkRate().equals(rate)) {
                return r;
            }
        }
        return null;
    }
}
