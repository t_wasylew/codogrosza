package main.java.servlets;

import main.java.dao.HashPassword;
import main.java.dao.UserDAO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import main.java.model.Rate;
import main.java.model.User;


@WebServlet(urlPatterns = "/userRegister")
public class RegisterServlet extends HttpServlet {
    private UserDAO userDAO = new UserDAO();
    private HashPassword hashPassword = new HashPassword();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            resp.sendRedirect("register.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String page;
        try {
            String name = req.getParameter("name");
            String login = req.getParameter("login");
            String password = req.getParameter("password");
            String hashedPassword = hashPassword.passwordHash(password);
            String incomeType = req.getParameter("incomeType");
            Double salary = Double.parseDouble(req.getParameter("incomeValue"));

            User userFromDB = userDAO.getUserByLogin(login);
            if (userFromDB != null) {
                page = "/register.jsp";
                String error = "loginTaken";
                req.setAttribute("error", error);
            } else {
                User newUser = new User();
                newUser.setName(name);
                newUser.setLogin(login);
                newUser.setPasswordHash(hashedPassword);

                Rate rate = new Rate();
                rate.setWorkRate(incomeType);
                rate.setRateValue(salary);
                rate.setUser(newUser);
                userDAO.addOrUpdateUser(newUser,rate);

                page = "/index.jsp";
            }
            req.getRequestDispatcher(page).forward(req, resp);
        } catch (IOException | ServletException exception) {
            exception.printStackTrace();
        }
    }
}

